from django.shortcuts import render, redirect
from User.forms import RegistrationForm, LoginForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from User.models import Post


# Create your views here.
def home(request):
    context = {'posts': Post.objects.all(), 'title': 'Home'}
    return render(request, 'home.html', context)


def about(request):
    context = {'title': 'About'}
    return render(request, 'about.html', context)


def login(request):
    form = LoginForm()

    context = {'form': form, 'title': 'Login'}
    return render(request, 'login.html', context)


def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account has been created for {username}')
            return redirect('login')
    else:
        form = RegistrationForm()

    context = {'form': form, 'title': 'Registration'}
    return render(request, 'register.html', context)


@login_required
def profile(request):
    context = {'title': 'Profile'}
    return render(request, 'profile.html', context)
