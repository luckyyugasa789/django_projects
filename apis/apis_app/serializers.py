from django.utils.datetime_safe import date
from apis_app.models import StringRegister
from rest_framework import serializers


# This class create Serialization and store data in database
class StringSerializer(serializers.ModelSerializer):
    class Meta:
        model = StringRegister
        fields = ['sentence']

    def save(self):
        result = StringRegister(
            sentence=self.validated_data['sentence'],
            created_on=date.today()
        )

        check_string = StringRegister.objects.filter(sentence=self.validated_data['sentence']).first()
        if check_string:
            raise serializers.ValidationError({'Text': 'This String already exist. Try another string'})

        result.save()
        return result
