from django.urls import path
from apis_app.views import string_save, show_all_strings

urlpatterns = [
    path('store/', string_save),  # store value in database
    path('show/all/sentences/', show_all_strings),  # Get all value from database
]
