from django.apps import AppConfig


class ApisAppConfig(AppConfig):
    name = 'apis_app'
