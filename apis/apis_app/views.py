from rest_framework.decorators import api_view
from apis_app.serializers import StringSerializer
from apis_app.models import StringRegister
from rest_framework.response import Response


# This function save a row in database using serialization class
@api_view(['POST'])
def string_save(request):
    if request.method == 'POST':
        serial = StringSerializer(data=request.data)
        data = {}
        if serial.is_valid():
            result = serial.save()
            data['id'] = result.id
            data['sentence'] = result.sentence
            data['created_at'] = result.created_on
        else:
            data = serial.errors

        return Response(data)


# this function get all sentence in your database
@api_view(['GET'])
def show_all_strings(request):
    sentences = StringRegister.objects.all().order_by('id').reverse()
    result = []
    for sentence in sentences:
        if sentence:
            data = {'id': sentence.id, 'text': sentence.sentence, 'created_at': sentence.created_on}
            result.append(data)

    if not result:
        data = {'message': 'no record found'}
        result.append(data)

    return Response(result)
