from django.db import models
from django.utils import timezone


# Create String Model
class StringRegister(models.Model):
    sentence = models.CharField(max_length=255, null=True)
    created_on = models.DateTimeField(null=True, default=timezone.now)

    def __str__(self):
        return self.sentence
