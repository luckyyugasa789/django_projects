from django.contrib.auth.hashers import make_password
from django.utils.datetime_safe import date
from rest_framework import serializers
from user_app.models import UserLogin
from werkzeug.security import generate_password_hash
import random


class RegistrationSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True)

    class Meta:
        model = UserLogin
        fields = ['first_name', 'last_name', 'email', 'password', 'password2']
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def save(self):
        account = UserLogin(
            email=self.validated_data['email'],
            first_name=self.validated_data['first_name'],
            last_name=self.validated_data['last_name'],
            username=self.validated_data['first_name'] + str(random.randint(0000, 9999)),
            password=make_password(self.validated_data['password']),
            created_on=date.today()
        )
        password = self.validated_data['password']
        password2 = self.validated_data['password2']

        if password != password2:
            raise serializers.ValidationError({'password': 'Password must be match.'})

        check_email = UserLogin.objects.filter(email=self.validated_data['email']).first()
        if check_email:
            raise serializers.ValidationError({'email': 'this email is already exist.'})

        account.set_password(password)
        account.save()
        return account
