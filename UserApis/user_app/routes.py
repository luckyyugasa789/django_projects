from django.urls import path
from user_app.views import registration_view, login, all_user, get_user, delete_user

urlpatterns = [
    path('register/', registration_view, name='register'),
    path('login/', login, name='login'),
    path('all/user/', all_user, name='all_user'),
    path('user/<user_id>', get_user),
    path('delete/<user_id>', delete_user),
]
