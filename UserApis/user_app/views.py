from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from user_app.serializers import RegistrationSerializer
from user_app.models import UserLogin
from django.conf import settings
from werkzeug.security import check_password_hash
from django.contrib.auth.hashers import make_password, check_password


@api_view(['POST'])
def registration_view(request):
    if request.method == 'POST':
        serializer = RegistrationSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            account = serializer.save()
            data['username'] = account.username
            data['first_name'] = account.first_name
            data['last_name'] = account.last_name
            data['email'] = account.email
            data['password'] = account.password
            data['created_on'] = account.created_on
        else:
            data = serializer.errors
        return Response(data)


@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    username = request.data.get("email")
    password = request.data.get("password")
    UserLogin = settings.AUTH_USER_MODEL
    # user = UserLogin.objects.get(email=username)
    user = UserLogin.objects.filter(email=username).first()
    data = {}
    if user and check_password_hash(user.password, password):
        data['username'] = user.username
        data['first_name'] = user.first_name
        data['last_name'] = user.last_name
        data['email'] = user.email
        data['password'] = user.password
        data['created_on'] = user.created_on
        return Response(data)
    else:
        return Response({"error": "Wrong Credentials"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
def all_user(request):
    # users = UserLogin.objects.all().order_by('-id')
    users = UserLogin.objects.all().order_by('id').reverse()
    output = []
    for user in users:
        if user:
            data = {'id': user.id, 'username': user.username, 'first_name': user.first_name,
                    'last_name': user.last_name, 'email': user.email,
                    'password': user.password, 'create_on': user.created_on}
            output.append(data)

    if not output:
        data = {'message': 'User not found!'}
        output.append(data)
    return Response(output)


@api_view(["GET"])
def get_user(request, user_id):
    user = UserLogin.objects.filter(id=user_id).first()

    if not user:
        return Response({'message': 'User not found!'})

    data = {'id': user.id, 'username': user.username, 'first_name': user.first_name, 'last_name': user.last_name,
            'email': user.email,
            'password': user.password, 'create_on': user.created_on}
    return Response(data)


@api_view(["DELETE"])
def delete_user(request, user_id):
    user = UserLogin.objects.filter(id=user_id).first()

    if not user:
        return Response({'message': 'User not found!'})

    elif user:
        data = {'id': user.id, 'username': user.username, 'first_name': user.first_name, 'last_name': user.last_name,
                'email': user.email,
                'password': user.password, 'create_on': user.created_on}
        return Response(data)

    user.delete()
    return Response({'message': 'User delete!'})
