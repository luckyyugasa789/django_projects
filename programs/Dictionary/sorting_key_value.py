key_value = {2: 56, 1: 2, 5: 12, 4: 24, 6: 18, 3: 323}

print("Sorting dictionaries are")

for i in sorted(key_value):
    print((i, key_value[i]), end=" ")

print('\n')

print(sorted(key_value.items(), key=lambda kv: (kv[1], kv[0])))
