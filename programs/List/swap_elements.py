_range = input("Enter the list range: ")

_list = []
[_list.append(input()) for i in range(0, int(_range))]
print('Before Swapping List', _list)

num1 = int(input('Enter the first position value: '))
num2 = int(input('Enter the Second position value: '))

if num1 >= 0 and num2 <= int(_range):
    _list[num1-1], _list[num2-1] = _list[num2-1], _list[num1-1]
    print('After Swapping List: ', _list)
else:
    print("Out of range")
